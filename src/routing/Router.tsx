import React from 'react';
import UnAuthenticatedRoute from './UnAuthenticatedRoute';
import AuthenticatedRoute from './AuthenticatedRoute';
import Home from '../pages/home'
import Login from '../pages/login';
import Register from '../pages/register';
import CreateResume from '../pages/createResume';
import {useDispatch} from 'react-redux';
import {setIsLoggedIn} from '../reducers/auth';

import {
    Switch,
    Route,
} from "react-router-dom";
import MyResumes from "../pages/myResumes";

// todo
// function NotFound() {
//     return <div>404 Not Found</div>;
// }

function Router() {
    const dispatch = useDispatch();
    dispatch(setIsLoggedIn(!!localStorage.getItem('access_token')))
    return (
        <>
            <Switch>
                <UnAuthenticatedRoute path="/login">
                    <Login/>
                </UnAuthenticatedRoute>
                <UnAuthenticatedRoute path="/register">
                    <Register/>
                </UnAuthenticatedRoute>
                <AuthenticatedRoute path="/add-resume">
                    <CreateResume/>
                </AuthenticatedRoute>
                <AuthenticatedRoute path="/all-resumes">
                    <MyResumes/>
                </AuthenticatedRoute>
                <Route path="/">
                    <Home/>
                </Route>
            </Switch>
        </>
    );
}

export default Router;
