import React, {ReactNode} from 'react';
import {Route, Redirect} from 'react-router-dom';
import {isLoggedIn} from '../reducers/auth';
import {useSelector} from "react-redux";

type Props = {
    children: ReactNode;
    path: string
}

function AuthenticatedRoute({children, ...rest}: Props) {
    const isAuthenticated = useSelector(isLoggedIn);

    return (
        <Route
            {...rest}
            render={({location}) =>
                isAuthenticated ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {from: location}
                        }}
                    />
                )
            }
        />
    );
}

export default AuthenticatedRoute;
