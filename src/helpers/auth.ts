export function isAuthenticated(): boolean {
    return !!localStorage.getItem('access_token');
}

export const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
