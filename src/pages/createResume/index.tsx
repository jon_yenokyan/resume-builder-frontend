import React, {useState} from 'react';
import FirstStep from './components/firstStep';
import SecondStep from './components/secondStep';
import {Store} from 'rc-field-form/lib/interface';

export enum VisibleScreen {
    first,
    second,
}

export enum EducationProps {
    type = 'education',
    title = 'Education',
    name = 'University Name',
    fakultet = 'Faculty',

}

export enum ExperienceProps {
    type = 'experience',
    title = 'Work Experience',
    name = 'Company',
    position = 'Position',
}

function CreateResume() {
    const [visible, setVisible] = useState<number>(VisibleScreen.first)
    const [formValues, setFormValues] = useState<Store>({});

    console.log(visible);

    const handleSetValues = (values: Store) => {
        setFormValues(values);
        setVisible(VisibleScreen.second);
    };

    return (
        <>
            <FirstStep visible={visible} onSetFormValues={handleSetValues}/>
            <SecondStep formValues={formValues} visible={visible} onSetVisible={setVisible}/>
        </>
    )
}

export default CreateResume;
