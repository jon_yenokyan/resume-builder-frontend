import React from 'react';

import {Document, Page, View, Text, Image, StyleSheet} from '@react-pdf/renderer'
import {ResumeInterface} from "../../resumeType";

const {PUBLIC_IMAGE_URL} = process.env;
const styles = StyleSheet.create({
    page: {backgroundColor: 'tomato'},
    section: {color: 'black', textAlign: 'center', margin: 30},
    text: {fontSize: 30, color: 'red', textTransform: "capitalize"},
    title: {fontSize: 50, color: 'green'},
    newSection: {backgroundColor: 'white'},
    components: {margin: 20, border: 1}
});
export default ({resume}: {
    resume: ResumeInterface;
}) => (
    <Document>
        <Page size="A4" style={styles.page}>
            <View style={styles.section}>
                <View>
                    {/*ToDo*/}
                    <Image style={{width: 150, height: 150}}
                           source={{
                               uri: "https://image.shutterstock.com/image-photo/white-transparent-leaf-on-mirror-260nw-1029171697.jpg",
                               method: "GET",
                               headers: {},
                               body: ''
                           }}
                    />
                </View>
                <View style={styles.newSection}>
                    <Text style={styles.text}>Name : </Text>
                    <Text> {resume.first_name} {resume.last_name}</Text>
                </View>
                <View style={styles.newSection}>
                    <Text style={styles.text}>City : </Text>
                    <Text> {resume.city}</Text>
                </View>
                <View style={styles.newSection}>
                    <Text style={styles.text}>Address : </Text>
                    <Text> {resume.address}</Text>
                </View>
                <View style={styles.newSection}>
                    <Text style={styles.text}>Phone : </Text>
                    <Text> {resume.phone}</Text>
                </View>
                {['armenian', 'english', 'russian'].map((key, index) => (
                    <View key={key} style={styles.newSection}>
                        <Text style={styles.text}>{key} : </Text>
                        <View style={{flexDirection: 'row', marginHorizontal: 'auto', width: '50%'}}>
                            <View style={{
                                width: 100 * (+[resume.armenian, resume.english, resume.russian][index]) / 5 + '%',
                                height: 20,
                                borderRadius: 10,
                                backgroundColor: 'red',
                            }}>
                            </View>
                            <View style={{
                                flex: 1,
                                backgroundColor: 'orange',
                                borderBottomRightRadius: 10,
                                borderTopRightRadius: 10
                            }}>
                            </View>
                        </View>
                    </View>
                ))}
                {resume.description && (

                    <View style={styles.newSection}>
                        <Text style={styles.text}>Description : </Text>
                        <Text> {resume.description}</Text>
                    </View>
                )}
                {resume.fb_link && (
                <View style={styles.newSection}>
                    <Text style={styles.text}>FB Link: </Text>
                    <Text> {resume.fb_link}</Text>
                </View>
                )}
                {resume.linkedin_link && (
                <View style={styles.newSection}>
                    <Text style={styles.text}>Linkedin Link: </Text>
                    <Text> {resume.linkedin_link}</Text>
                </View>
                )}
                {resume.telegram && (
                <View style={styles.newSection}>
                    <Text style={styles.text}>Telegram Link: </Text>
                    <Text> {resume.telegram}</Text>
                </View>
                )}
            </View>
        </Page>
        <Page size="A4" style={styles.page}>
            <View style={styles.section}>
                <View style={styles.newSection}>
                    <View style={styles.newSection}>
                        <Text style={styles.title}>Skills:</Text>
                    </View>
                    <View style={[styles.components, {flexDirection: 'row'}]}>
                        <Text>
                            {resume.skills.map((skill) => skill.name).join(' / ')}
                        </Text>
                    </View>
                </View>

                <View style={styles.newSection}>
                    <View style={styles.newSection}>
                        <Text style={styles.title}>Education:</Text>
                    </View>
                    <View>
                        {resume.education.map((item) => (
                            <View key={item.id} style={styles.components}>
                                <View>
                                    <Text style={styles.text}>Name: </Text>
                                    <Text> {item.name}</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>Fakultet: </Text>
                                    <Text> {item.fakultet}</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>Address: </Text>
                                    <Text> {item.address}</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>From Date: </Text>
                                    <Text> {item.from_date}</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>To Date: </Text>
                                    <Text> {item.to_date}</Text>
                                </View>
                            </View>
                        ))}
                    </View>
                </View>

                <View style={styles.newSection}>
                    <View style={styles.newSection}>
                        <Text style={styles.title}>Work Experience:</Text>
                    </View>
                    <View>
                        {resume.experience.map((item) => (
                            <View key={item.id} style={styles.components}>
                                <View>
                                    <Text style={styles.text}>Name: </Text>
                                    <Text> {item.name}</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>Position: </Text>
                                    <Text> {item.position}</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>Address: </Text>
                                    <Text> {item.address}</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>From Date: </Text>
                                    <Text> {item.from_date}</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>To Date: </Text>
                                    <Text> {item.to_date}</Text>
                                </View>
                            </View>
                        ))}
                    </View>
                </View>
            </View>
        </Page>
    </Document>
);

