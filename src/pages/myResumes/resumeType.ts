export interface ResumeInterface {
    id: number;
    first_name: string;
    last_name: string;
    address: string;
    img_path: string;
    city: string;
    phone: string;
    armenian: string;
    russian: string;
    english: string;
    description?: string;
    fb_link?: string;
    linkedin_link?: string;
    telegram?: string;
    created_at: string;
    updated_at: string;
    skills: {
        id: number;
        name: string;
    }[];
    experience: {
        id: number;
        name: string;
        position: string;
        address: string;
        from_date: string;
        to_date: string;
    } [];
    education: {
        id: number;
        name: string;
        fakultet: string;
        address: string;
        from_date: string;
        to_date: string;
    }[];

}
