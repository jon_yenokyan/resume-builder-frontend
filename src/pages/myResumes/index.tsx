import React, {useState, useEffect} from 'react';
import {List, Avatar, Button, Skeleton, Modal, Spin} from 'antd';

import PDFResumeView from './components/pdfResumeView';
import { PDFDownloadLink, PDFViewer} from "@react-pdf/renderer";
import {ResumeInterface} from "./resumeType";

const {REACT_APP_SERVER_URL, REACT_APP_PUBLIC_IMAGE_SERVER} = process.env;

function MyResumes() {
    const [showResume, setShowResume] = useState<ResumeInterface>();
    const [isLoading, setLoading] = useState(false);
    const [resumes, setResumes] = useState<ResumeInterface[]>([]);

    const init = async () => {
        setLoading(true);
        const response = await fetch(REACT_APP_SERVER_URL + '/resume', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });

        const result = await response.json();
        setLoading(false);
        setResumes(result.data);
    };

    useEffect(() => {
        init().then();
    }, []);

    const deleteResume = async (id: number) => {
        await fetch(REACT_APP_SERVER_URL + '/resume/' + id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });

        await init();
    };

    return (
        <>
            <Modal
                visible={!!showResume}
                footer={null}
                onCancel={() => setShowResume(undefined)}
            >
                {showResume ? (
                    <>
                        <PDFDownloadLink document={<PDFResumeView resume={showResume}/>} fileName="resume.pdf">
                            {({ loading }) =>
                                loading ? "Loading document..." : "Download Pdf"
                            }
                        </PDFDownloadLink>
                        <PDFViewer width={420} height={700}>
                            <PDFResumeView resume={showResume}></PDFResumeView>
                        </PDFViewer>

                    </>
                ) : null}
            </Modal>
            {isLoading ? <Spin/> :
                <List
                    size="large"
                    className="resume-list"
                    itemLayout="horizontal"
                    dataSource={resumes}
                    renderItem={(item) => (
                        <List.Item
                            actions={[
                                <a onClick={() => {
                                    setShowResume(item);
                                }}>
                                    show
                                </a>,
                                <a onClick={() => deleteResume(item.id)} key="list-loadmore-more">delete</a
                                >]}
                        >
                            <Skeleton avatar title={false} loading={false} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src={item.img_path && (REACT_APP_PUBLIC_IMAGE_SERVER+item.img_path) }/>
                                    }
                                    title={item.first_name + ' ' + item.last_name}
                                />
                            </Skeleton>
                        </List.Item>
                    )}
                />
            }
        </>
    )
}

export default MyResumes;
