import React from 'react';
import {Form, Input, Button, message} from 'antd';
import {Store} from 'rc-field-form/lib/interface';
import {emailPattern} from '../../helpers/auth';
import {useDispatch} from 'react-redux';
import {setIsLoggedIn} from '../../reducers/auth';
import {useHistory} from 'react-router-dom';

const {useForm} = Form;
const {REACT_APP_SERVER_URL} = process.env;

const layout = {
    labelCol: {span: 10},
    wrapperCol: {span: 14},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function Register() {
    const dispatch = useDispatch();
    const history = useHistory();

    const [form] = useForm();
    const onFinish = async (values: Store) => {

        const {name, email, password, password_confirmation} = values;
        const formData = new FormData();
        formData.append('name', name);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('password_confirmation', password_confirmation);
        try {
            const response = await fetch(REACT_APP_SERVER_URL + '/auth-register', {
                method: 'POST',
                body: formData,
                headers: {
                    'Accept': 'application/json'
                }
            });

            const result = await response.json();
            if (response.status !== 200) {
                message.error(result.message);
                return;
            }
            localStorage.setItem('access_token', result.access_token);
            dispatch(setIsLoggedIn(!!result.access_token))
            history.push("/");
        } catch (error) {
            message.error('Something went wrong');
        }
    };

    return (
        <Form
            form={form}
            {...layout}
            onFinish={onFinish}
            name="register"
        >
            <Form.Item
                label="Name"
                name="name"
                rules={[
                    {required: true, message: 'Please input your name!'},
                    {min: 2, message: 'Minimum 2 characters.'},
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {required: true, message: 'Please input your email!'},
                    {
                        pattern: emailPattern,
                        message: 'Invalid email.'
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {required: true, message: 'Please input your password!'},
                    {min: 6, message: 'Minimum 6 characters.'},
                ]}
            >
                <Input.Password/>
            </Form.Item>
            <Form.Item
                label="Confirm Password"
                name="password_confirmation"
                rules={[
                    {
                        required: true,
                        message: 'Please confirm your password!',
                    },
                    ({getFieldValue}) => ({
                        validator(rule, value) {
                            if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                            }
                            return Promise.reject('Passwords aren\'t match');
                        },
                    }),
                ]}
            >
                <Input.Password/>
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}

export default Register;
